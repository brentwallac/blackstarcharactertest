// ConsoleTest.cpp : Defines the entry point for the console application.
// Console Test is designed to prototype the relationships between various characters based off 
// triggered events. 
// We want to test how certain characters will respond based off certain conditions. i.e. stat impacts on health vs being shot. 
/* IN order to do this we're going to replicate a game-loop that takes our characters and randomly generates scenarios. */

#include "stdafx.h"
#include "Character.h"
#include <iostream>
#include "Engine.h"
#include <time.h>
#include <vector>
#include "DataSource.h"


using namespace std;
int main()
{
	/* Number of characters we want to generate */
	const int NUM_CHARACTERS = 1;

	// Our character queue used for the game engine loop
	vector<Character*> charactersQueue;
	cout << "Welcome to BlackStar relationship system test " << std::endl;
	cout << "We'll now load you a new party of characters" << std::endl;

	// Create our log object & initialise it
	DataSource datalog;

	std::vector<string> MainLog;

	//MainLog.push_back("This is our mainlog test");

	vector<map<string, string>> characterSheetValues;
	
	characterSheetValues = datalog.CSVToMap();

	if (characterSheetValues.empty())
	{
		cerr << "No character sheet data loaded. Check your input data, pal." << endl;
		system("pause");
		return 1;
	}
	// Loop through the vector maps and create a new character
	int indexVal = 0;
	for (auto it = characterSheetValues.begin(); it != characterSheetValues.end(); it++,indexVal++)
	{
		// We create a new object directly into the queue to avoid memory issues with pointers.
		charactersQueue.push_back(new Character());
		charactersQueue.at(indexVal)->MapCharacterSheet(&*it);
		cout << "Welcome " << charactersQueue.at(indexVal)->GetName() << endl;
		
	}
	

	// Init. our game engine with the characters created
	Engine GameEngine(&charactersQueue);
	// Start the game loop.
	GameEngine.GameLoop();

	// Sometimes the console closes immediately, so use this to artifically stop it from closing.
	system("pause");
	return 0;

}
