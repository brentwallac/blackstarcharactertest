#pragma once
#include "Character.h"

class Enemy
{
public:
	Enemy();
	
	void TakeDamage(float damageAmount);
	/* When the actor health < 0*/
	void Destroy();
	/* The damage an enemy does */
	float Attack();

	float enemyHealth = 20.0f;
	~Enemy();
	std::string Name; 
	// Setter for bSetToDelete
	inline void SetToDelete() { bSetToDelete = true; }
	// Getter for bSetToDelete
	inline bool GetSetToDelete() { return bSetToDelete; }
	// Assess the enemy health that this character can see. Return the highest health
	class Character* AssessEnemyHealth(std::vector<Character*> *characterList);
private: 
	// bool value that determines if this instance gets deleted
	bool bSetToDelete = false;
	// Sets delete 
};

