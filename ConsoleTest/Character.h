#pragma once
#include <vector>
#include <string>
#include <map>
#include "Enemy.h"
#include "SetupCharacter.h"

// Enum for our weapon
enum weaponclass {
	WPSMALL,
	WPBIG,
	WPEXPLOSIVE,
	WPDEVICE,
	WPMELEE
};

// Struct for a standard weapon
struct weapon {
	enum weaponclass gunclass;
	string name;
	int damage_ceil;
	int damage_floor;
	int range;
	int damage_modifier;


};
using namespace std;
/*
* Our character class consists of a series of variables and a string.
* This class manages our Character's default and base values.
*
* Each character has an action; Attack, Move, Heal.. etc 
* This action is accessed via the queue and then put into the event action queue via the game engine
*/
/* Each action can either be broken down into three 'types' */
enum eActionType {
	Move,
	Attack,
	Idle
};
class Character
{
public:
	Character();
	/* 
	* The attribute array for a character. Maybe this is better somewhere gloally? 
	* TODO: See about using a struct instead of an array of strings
	* TODO: Maybe locate these attributes somewhere globally or somewhere more static?
	*
	*/
	const vector <string> AttributeArray = { "brain","muscles","hands", "lungs","heart","mouth","eye" };
	const vector <string> SkillArray = { "stealth","small_guns","heavy_guns","mechanics", "explosives","unarmed","security","medicine" };
	const vector <string> SpectrumArray = { "mental","physical","teamwork" };

	// The weapon for our character 
	weapon primaryWeapon = { WPSMALL,"SMG",20,15,40,0 };

	// Our Stats for our character. 
	void MapCharacterSheet(std::map < std::string, std::string > *CharacterSheet);
	
	/* Returns the specific attribute */
	int GetAttribute(string  Attribute);

	/* Assigns a randomized set of attributes, skills and spectrums for the character*/
	void GenerateSheet();

	/*Setter for our Attribute values*/
	void SetAttribute(string Attribute, float value);

	/* Returns the name */
	inline string GetName() { return characterName; }

	/* Takes in an enemy and applies attack damage */
	void AttackEnemy(class Enemy *enemy);

	/*Prints out the character attribute*/
	void PrintAttributes();
	
	/* Setting for character actions*/
	void SetAction(enum eActionType action);

	/* Setter for character's health */
	void SetHealth(float new_value);

	/* Getter for Skill */
	int GetSkill(string skill);

	/* Setter for a skill */
	void SetSkill(string skill, float value);

	/* Inline function for setting character's health */
	inline float GetHealth() { return health; }

	/* Get the action the current character is invoking*/
	enum eActionType GetAction();

	// Get the currentAction as a string value
	string GetActionAsString(); 

	/* Set value for Spectrum */
	void SetSpectrum(string attribute, float value);
	/* Setter for the spectrum */
	int GetSpectrum(string specrum);
	~Character();
	
	/* Action Updated marker: signals that this character has changed their action. Used to 
	* used during the event loop determining if the character is doing anything different. 
	*/
	bool HasActionUpdated();
	// Setter for our ActionUpdated variable
	inline void SetActionUpdated(bool bHasUpdated) { bActionUpdated = bHasUpdated; }
	// Default stage for action
	map<string, float>* GetSkillMap();
	map<string, float>* GetSpectrumMap();
	map<string, float>* GetAttributeMap();
	
protected: 

private:
	
	/* The current action the character is doing*/
	enum eActionType currentAction = Idle;
	// Is set to true on default
	bool bActionUpdated = true;
	string characterName;

	float health = 30.0;
	map<string, float> AttributeMap;
	map<string, float> SkillMap;
	map<string, float> SpectrumMap;
};

