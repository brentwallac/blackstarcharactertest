/* SetupCharacter Class:
 Sets up a character with some variables based off base attributes
*/

#pragma once
#include <array>
#include <vector>
#include <map>
using namespace std;
class SetupCharacter 
{
public:
	SetupCharacter();

	/*
	* Takes in array of standard attributes and outputs vector
	* @param const vector <string> [should contain values like 'health' etc] The base attributes a character has as strings
	* @param map<string,int>& a reference to a character's attributes
	* @return A character's randomized attributes - map<string,int>
	*/
	std::map<string, float> AssignAttributes(const vector<string>& BaseAttributesTypes, std::map<std::string,float>& AttributeMap,float randFloor,float randCeil);
	string CreateName();
	~SetupCharacter();
private:
	// Sets up random values for our character. 
	

	/*Randomizes our attribute to some pre-defined amount*/
	int RandomizeAttribute(float Floor = 1.0f, float Ceiling = 10.0f);
	/* Randomly generates a first and last name from some arbitary list of strings*/
	

	const string first_names[10] = { "Jimmy","Cracker","Gus","Sharon","Mortimer","Andrew","Devvilo","Luigi","Mario" };
	const string last_names[10] = { "Smith","Johnson","Davis","Miller","Roderick","Talbert","Mikko","Smoosh","Carter" };
};

