/*
* The MIT License (MIT)
*
* Copyright (c) 2016 Mario Badr
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.

// this has been modified to suit the purposes of this project, however the 
// source is https://gist.github.com/mariobadr/673bbd5545242fcf9482
// the why is here - https://www.reddit.com/r/gamedev/comments/41v2td/a_modern_c_game_loop_template_mit/
// And more info here - http://www.fabiensanglard.net/timer_and_framerate/index.php
*/
#include "stdafx.h"
#include "Engine.h"
#include "iostream"
#include <ctype.h>
#include <random>
#include "Enemy.h"
#include "Menu.h"


using namespace std::chrono_literals;
constexpr std::chrono::nanoseconds timestep(16ms);
const int ROUND_TIME = 3;
Engine::Engine(vector<Character*> *characters)
{
	characterList = characters;
	cout << "Setting up new characters. "<< endl;
	roundTime = std::chrono::seconds(ROUND_TIME);

}

Engine::~Engine()
{
}

float Engine::CalculateToHitChance(Character *Attacker)
{
	// Some values that should be part of the enemy 
	int enemyStance = 1;
	int characterStance = 1;
	int characterModifiers = 0; // Largely ignored for now, but will relate to other char. bonuses or whatever. 
	int characterElevation = 1;
	std::random_device rd;

	std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
	
	//Bug found: if small guns / 2 + eye * 2 < small_guns then we get a min / max issue with randomisation.
	//
	//

	//std::cout << "Small gun test min" << (Attacker->GetSkill("small_guns") / 2) + (Attacker->GetAttribute("eye") * 2) << std::endl;
	//std::cout << "Small gun test max " << Attacker->GetSkill("small_guns") << std::endl;

	std::uniform_int_distribution<int> uni( (Attacker->GetSkill("small_guns") / 2) + ( Attacker->GetAttribute("eye") * 2), Attacker->GetSkill("small_guns")); // guaranteed unbiased
	
	int weaponRangeResult = Attacker->primaryWeapon.range - enemyLocation.distance;

	if (weaponRangeResult > 0) 
	{
		// The gun is in range, but we don't want to give any bonuses for the gun to be in range. However, we do want to 
		// decrease for the gun being out of range proportional to the distance vs the gun range. 
		weaponRangeResult = 0;
	}

	float attackPosition = (Attacker->GetAttribute("eye") * 2) + (static_cast<float>(uni(rng)) + Attacker->GetSpectrum("mental") + Attacker->GetSpectrum("physical") + Attacker->primaryWeapon.damage_modifier + characterModifiers) + (characterElevation * 10) - (characterStance * 5) + weaponRangeResult;
	
	float defencePosition = enemyLocation.coverlevel + (enemyLocation.distance + 0 + 0) + (2 * enemyLocation.lightlevel) + (enemyLocation.elevation * 10) - (enemyStance * 5);
	
	cout << "Attacker: " << attackPosition << endl;
	cout << "Defender: " << defencePosition << endl;

	return attackPosition - defencePosition;
	
}
void Engine::EnemyDestroyQueue()
{
	vector<Enemy*>::iterator it = enemyList.begin();

	while (it != enemyList.end()) 
	{
		if ((*it)->GetSetToDelete()) {
			// Destroy the enemy actor as he dead. 
			(*it)->Destroy();

			it = enemyList.erase(it);
		}
		else {

			it++;
		}
		
	}
}
// we use a fixed timestep of 1 / (60 fps) = 16 milliseconds
// Maybe I need some kind of queue system or maybe pull in the game_state ?
bool Engine::HandleEvents() 
{
	vector<Character*> Char = *characterList;

	std::cout << "Total Enemies Left: " << enemyList.size() << " " << std::endl;

	for (auto it = Char.begin(); it != Char.end(); it++) {

		// Run through what each action will do 		

		if (enemyList.empty()) 
		{

			cout << "All enemies dead " << endl;

			return true;
		}
		else
		{

			float result = CalculateToHitChance((*it));

			if (result > 0) {
				// Calculate Damage
				float DamageApplied = CalculateDamage((*it));

				// Find the next available Enemy in the vector 
				Enemy* enemy = enemyList.front();

				// Check if valid pointer
				if (enemy != nullptr) 
				{

					cout << (*it)->GetName() << " Shoots at " << enemy->Name << endl;
					cout << "Damage done to " << enemy->Name << " (Enemy) was ( " << DamageApplied << " ) " << endl;

					if (enemy->enemyHealth > 0) 
					{

						enemy->TakeDamage(DamageApplied);

						if (enemy->enemyHealth <= 0)
						{

							std::cout << enemy->Name << " is dead! " << endl;

						}
						else
						{
							cout << enemy->Name << " is on " << enemy->enemyHealth << endl;
						}
					}
				}
			}
			else
			{
				cout << (*it)->GetName() << " has missed! " << endl;
			}

		}

	}

	// Check if enemy is alive and then run the enemy logic
	EnemyDestroyQueue();
	EnemyTurn();

	return false;
}

void Engine::EnemyTurn()
{
	cout << "Enemy Turn " << endl;
	Character *character;
	if (!enemyList.empty())
	{
		for (auto it = enemyList.begin(); it != enemyList.end(); it++)
		{

			if (!characterList->empty())
			{
				// Get the first character out of the list
				Character* targetedCharacter = characterList->front();

				if (targetedCharacter)
				{

					// Could return a null ptr
					//character = (*it)->AssessEnemyHealth(characterList);
					targetedCharacter->SetHealth(targetedCharacter->GetHealth() - (*it)->Attack());

					cout << targetedCharacter->GetName() << " shot and left with " << targetedCharacter->GetHealth() << endl;


				}
			}
			else
			{
				cout << "All characters dead";
			}
		}
	}
}

float Engine::CalculateDamage(Character *Attacker)
{
	std::random_device rd;

	std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)

	std::uniform_int_distribution<signed int> uni(Attacker->primaryWeapon.damage_floor, Attacker->primaryWeapon.damage_ceil);

	// Convert skill to float as it won't be divisible by 100 without resulting in 0 due to it being an skill being an int.
	return (uni(rng) * (static_cast<float>(Attacker->GetSkill("small_guns")) / 100) + Attacker->primaryWeapon.damage_modifier);
}
void Engine::Render() 
{
	std::cout << "Start of round: " << roundCount << std::endl;

}

void Engine::GameLoop() 
{
	using clock = std::chrono::high_resolution_clock;
	
	auto time_start = clock::now();
	// Generate a few enemies 
	for (int x = 0; x < NUM_ENEMIES; x++) {

		cout << "Creating enemy character.." << endl;
		enemyList.push_back(new Enemy());
	}
	
	bool quit_game = false;

	std::cout << "Time per round " << roundTime.count() << endl;

	while (!quit_game) {
		
		auto delta_time = clock::now() - time_start;
		
		if (std::chrono::duration_cast<std::chrono::nanoseconds>(delta_time % roundTime).count() == 0 ) {
			
			roundCount++;
			//time_start = clock::now(); // Reset the clock. I don't wany any floating-point number issues
			Render();

			quit_game = HandleEvents();

		}
		
		
		// Check the round time 

	
	}
}
