#include "stdafx.h"
#include "SetupCharacter.h"
#include <random>
#include <string>
#include <array>
#include <iostream>
#include <map>

using namespace std;
SetupCharacter::SetupCharacter()
{
	// Creates a new character with random variables
	
}
	
map<string,float> SetupCharacter::AssignAttributes(const vector<string> &BaseAttributesTypes,std::map<std::string,float> &AttributeMap,float randFloor,float randCeil){
	
	//string *BaseAttributes = new string[5];
	for (auto it = BaseAttributesTypes.begin(); it != BaseAttributesTypes.end(); it++) {

		AttributeMap[*it] = static_cast<float>(RandomizeAttribute(randFloor,randCeil));
	}

	return AttributeMap;
}

// Randomizes our character
int SetupCharacter::RandomizeAttribute(float Floor, float Ceiling)
{
	std::random_device rd;

	std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
	std::uniform_int_distribution<int> uni(Floor, Ceiling); // guaranteed unbiased

	return uni(rng);


}

string SetupCharacter::CreateName() {
	std::random_device rd;

	std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
	std::uniform_int_distribution<int> fname_index(1, static_cast<int>(first_names->size())); // guaranteed unbiased
	std::uniform_int_distribution<int> lname_index(1, static_cast<int>(last_names->size())); // guaranteed unbiased
	return first_names[fname_index(rng)] + " " + last_names[lname_index(rng)];
}
SetupCharacter::~SetupCharacter()
{
}
