#pragma once
#include <chrono>

/* Each action can either be broken down into three 'types' */
enum eActionType {
	Move,
	Attack,
	Idle
};

/* Actions are used by characters to make changes to their state. 
* These are a base classes that are then invoked by other classes. 
*
*/
class Action
{

public:
	Action();

	~Action();

protected: 
	/* Return the current action */
	virtual eActionType GetCurrentAction();
	/* Set the current action */
	virtual void  SetCurrentAction(eActionType newAction);

private:
	eActionType currentActionType = Idle;
};
