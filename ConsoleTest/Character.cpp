#include "stdafx.h"
#include <iostream> //cout cin
#include <map> // Map
#include <time.h>
#include <vector>
#include "Character.h"


Character::Character()
{
	// Generate an arbitary weapon
	weapon primaryGun;

	
	/* Setup all our default values. Will most likely be defined by the player with certain
	limitations. I wonder if this is best way to approach accessing various attributes via an array / map. They use setters / getters, and it's awfully redundant code. 
	TODO: investigate other - possibly better - solutions
	*/

}

void Character::GenerateSheet()
{
	SetupCharacter CharacterEngine;

	CharacterEngine.AssignAttributes(AttributeArray, AttributeMap, 1.0f, 10.0f);

	CharacterEngine.AssignAttributes(SkillArray, SkillMap, 40.0f, 100.0f);

	CharacterEngine.AssignAttributes(SpectrumArray, SpectrumMap, -5.0f, 5.0f);
	// Assign name to generated character string
	characterName = CharacterEngine.CreateName();
}

void Character::MapCharacterSheet(std::map <std::string, std::string> *CharacterSheet) {
	
	map <std::string,std::string> DefCharacterSheet = *CharacterSheet;

	for (auto it = DefCharacterSheet.begin(); it != DefCharacterSheet.end(); it++)
	{
		
		//AssignAttributeKeyToMap(&(*it), &AttributeArray);
		for (auto index = AttributeArray.begin(); index != AttributeArray.end(); index++)
		{

			
			// Does our attribute areray match the key in the character sheet iterator?
			if (*(index) == it->first) {
			
				AttributeMap[*(index)] = static_cast<float>(atof(it->second.c_str()));
			}
		}
		 
		for (auto index = SkillArray.begin(); index != SkillArray.end(); index++)
		{
			// Does our attribute array match the key in the character sheet iterator?
			if (*(index) == it->first) {
				SkillMap[*(index)] = static_cast<float>(atof(it->second.c_str()));
			}
		}
		 
		for (auto index = SpectrumArray.begin(); index != SpectrumArray.end(); index++)
		{
			// Does our attribute array match the key in the character sheet iterator?
			if (*(index) == it->first) {
				SpectrumMap[*(index)] = static_cast<float>(atof(it->second.c_str()));
			}
		}
		// Assign our name
		if ("name" == it->first) {
			cout << "Name being assigned: " << it->second << endl;
			characterName = it->second;
		}

	}
}
void Character::SetSpectrum(string attribute, float value) 
{
	SpectrumMap[attribute] = value;
}


string Character::GetActionAsString() {

	switch (currentAction) {
		case  Move:
			return "Move";
		break;

		case Attack:
			return "Attack";
		break;

		case Idle:
			return "Idle";
		break;
		default:
			return "Idle";
	}
}

int Character::GetSpectrum(string spectrum)
{
	return SpectrumMap[spectrum];
}

int Character::GetSkill(string skill) 
{

	return SkillMap[skill];
}

void Character::SetSkill(string attribute, float value)
{
	SkillMap[attribute] = value;
}

void Character::SetAttribute(string Attribute, float value) 
{
	AttributeMap[Attribute] = value;
}

int Character::GetAttribute(string Attribute) 
{
	return AttributeMap[Attribute];
}

void Character::SetHealth(float new_value) 
{
	health = new_value;

}

void Character::AttackEnemy(Enemy * enemy)
{
}

void Character::PrintAttributes()
{

	for (auto it = AttributeArray.begin(); it != AttributeArray.end(); it++)
	{
		cout << *it << " - " << AttributeMap[*it] << '\n';
	}
}


void Character::SetAction(enum eActionType action) 
{

	currentAction = action;
	// Notify our character that their action has updated
	bActionUpdated = true;
}


enum eActionType Character::GetAction() 
{
	return currentAction;
}



Character::~Character()
{
}

bool Character::HasActionUpdated()
{
	return bActionUpdated;
}

map<string, float>* Character::GetSkillMap()
{
	return &SkillMap;
}

map<string, float>* Character::GetSpectrumMap()
{

	return &SpectrumMap;
}

map<string, float>* Character::GetAttributeMap()
{
	return &AttributeMap;
}

