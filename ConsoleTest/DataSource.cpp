#include "stdafx.h"
#include "DataSource.h"
#include <chrono>
#include <algorithm>
#include <time.h>


DataSource::DataSource()
{	
	// Create CSV for the output of all our data to be reviewed
	
}


bool DataSource::WriteOutputToCSV(std::string stringValue) 
{
	dataout.open(csvlocation.c_str(), std::ofstream::out | std::ofstream::app);

	if (dataout.is_open())
	{
		dataout << "Name" << "," << stringValue << std::endl;

		dataout.close();

		return true;
	}
	else 
	{
		std::cerr << "csv file is not open." << std::endl;

		return false;
	}
	
}
// For multiple characters this should return a vector of std::map<std::string,std::string>
std::vector<std::map<std::string,std::string>> DataSource::CSVToMap()
{
	datacharacter.open(charactersheetimport.c_str());

	std::string line;
	std::vector<std::map < std::string, std::string >> tmpMap;
	
	if (datacharacter.is_open()) {

		// Method below takes string, finds the separator and converts to key and value for map
		std::map <std::string, std::string > tmpCharacter;
		while (getline(datacharacter, line)) 
		{
			
			std::size_t found = line.find_first_of(',');
			
			if (found != std::string::npos) {
				// TODO: Check if line contains the value 'name' so we know it's a 
				// a new character, and then assign that accordingly. 
				
				// Get the Key value from our string
				std::string tmpKey = line.substr(0, found);
				// Convert our string to lower case for safer name checking
				std::transform(tmpKey.begin(), tmpKey.end(), tmpKey.begin(), ::tolower);
				
				if (tmpKey == "name") 
				{
					
					// A way to determine if this is our first character. If so, then we'll not bother creating a new character

					// If our character isn't empty, then there must already be character attributes preceeding this. Lets add them to the list and then empty out our character
					if (!tmpCharacter.empty())
					{
					
						tmpMap.push_back (tmpCharacter);

						// Clear out our tmpCharacter
						tmpCharacter.clear(); 
					}
					
					
				}
				// Get the substring & value from the string determined by ","
				std::string tmpSubstr =  line.substr(found + 1, line.length());
				std::string tmpVal = tmpSubstr.c_str();
			
				// Add to our Character Vector
				tmpCharacter[tmpKey] = tmpVal;
			}
			// Get the position 
			
		}
		// Add our last character to the map
		tmpMap.push_back(tmpCharacter);

		datacharacter.close();
	}
	else
	{
		std::cerr << "File cannot import - unopened" << std::endl;
	}
	return tmpMap;
}
bool DataSource::WriteOutputToCSV(std::map<std::string,float> *file)
{

	std::map<std::string, float> tmpMap = *file;
	dataout.open(csvlocation.c_str(), std::ofstream::out | std::ofstream::app);

	if (dataout.is_open())
	{
		
		for (const auto& elem : *file)
		{
			dataout << elem.first << "," << elem.second << std::endl;
		}
		dataout << " " << " " << std::endl;
		dataout.close();
	}
	else
	{
		std::cerr << "csv file is not open." << std::endl;
	}

	

	return false;
}

/* Note - Visual Studio bug; ensure Solutions Explorer is turned off!! It prevents writing to files*/
bool DataSource::WriteToLog(std::vector<std::string> file)
{
	logfile.open(logLocation, std::ofstream::out | std::ofstream::app);

	if (logfile.is_open())
	{
		for (std::vector<std::string>::const_iterator i = file.begin(); i != file.end(); ++i)
		{
			logfile << *i << std::endl;
		}

		logfile.close();

		return true;
	}
	else
	{
		std::cerr << "logfile is not open." << std::endl;

		return false;
	}
	

}

DataSource::~DataSource()
{
}
