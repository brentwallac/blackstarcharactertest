﻿#BlackStar: Design documents#

###The Story###
The year is 2240. The human race has outgrown Earth and has forced many to seek new opportunities on planets and moons lightyears away. The new frontier has provided an opportunity for people of all types and creeds to create new civilizations and continue the legacy of their home planet, Earth. 

Naturally, where there is opportunity to build and prosper, there is also opportunity to destroy and pillage. The further a colony gets from earth, the poorer the governance and policing. In response, colonies have been forced to defend themselves with militas of their own, with makeshift ships and weapons created with  limited resources at their disposal. For some colonies, raising an army and defending themselves has been enough to keep the proverbial wolves away from their quaint doors. For others, they have neither the means nor the time to defend themselves. One such colony is where you, the head of a mercenary company BlackStar, step in.


##Purpose##
BlackStar is a PC game that is inspired from early Strategic RPGs such as Jagged Alliance, Xcom and early Bioware games like Baldurs's Gate and Fallout. The objective of the game is to form and manage a squad, each containing different individuals with various strengths and weaknesses to overcome enemy combatants in a number of environments. 

Like Jagged Alliance, Blackstar is broken down into various coordinates; think of a 16 x 16 grid. These coordinates translate into a 'game map' and  will feature specifically designed environments ranging from arid deserts to ice-laden tundra's through to towns of varying sizes and types.

##Gameplay##
Blackstar is easiest to describe as being two 'layers' of strategic play. At the top-most layer, the game is about management of your crew, their needs and the ebb and flow of the stategic game map & your response there in. It also includes the management of each sector's defense and financial management.  

The second layer is the individual maps themselves, the combat areas with enemies and obstacles to reapture territory. 

##AI and Enemy##

The AI ideology of the enemies in BlackStar is scripted based. Some enemies will target various characters depending on their stats, visibility, chance to hit and other environmental concerns. 

The AI will 
##The Game Loop ##

In it's simplest form, the game loop consists of: 
1. Selecting a crew 
2. Assigning an area to traverse on the grid map
3. Entering the chosen area and exploring it via the tactical map 
4. Gathering the weapons, resources and and applying those to your current crew - be it via training, gear purchases or 'fixes'. 

## Crew ## 

At its heart, Blackstar is about the management of your crew. A crew is made up by the player selecting a number of pre-defined mercenaries. Each mercenary has a set of base stats and some internal values that makes up their personality.  

Each character's skills can be divided into three groups of variables that impact various parts of the game

###Spectrum### 
A spectrum is an internal value not seen by the player. Specrums impact a character's ability to function, whether alongside other characters or simply performing during the stresses of combat. There are three types of 'spectrums'. Spectrums are not hard variables. Instead they waiver between -1 to 1. though for simplicity of coding it'll be 0 to 10, with 5 being netural

1. Physical - The character's perceived physical state. This can change dramatically if a character is wounded.  

2. Mental - The character's mental state. Emotional states can cause characters to become afraid, stay cool under fire and freak out. Mental state can give buffs to all manner of attributes and skills.

3. Teamwork - Some characters form bonds or attachments with other characters. These can be positive or negative experiences and can impact performance in battle. In extreme cases, it can cause characters to hurt each other or simply leave. These give off minor buffs and blend with Charisma. Charisma is also paramount in decreasing the negatives and increasing the positives. 

Consider spectrums to have an overarching impact on how a character interacts with their situation, the environment and other characters. 

###Attributes###
Max Attribute Points 25

Brain 	- The smarts of an individual. Essential for those doing things like Hacking and keeping your mental state in-check.
Muscles - The physical abilities of someone to lift heavy things, unarmed capabilities. 
Hands  	- How quick someone is in making reactions / hand-eye coordination.
Lungs	- Your fitness ability, how long you can run for before breaking down into a mess. 
Heart 	- How tough you are, Hit points, ability to resist damage and overall grit.
Mouth 	- The raw ability someone has to be influential. Handy for training, helping others and operating in a team etc.  
Eye 	- How good at you from seeing.

###Skills###
Max Skill Points 100

Small Guns 	| A proficieny in shooting pistols, shotguns, and some light rifles.
Big Guns 	| Big guns include sniper rifles, heavy weapons, machine guns and rocket launchers. (STR )
Security 	| Ability to hack into systems, lockpick disable alarms and traps. (INT)
Stealth 	| The ability to move without being detected. (DEX + INT)
Explosives  | The handling of grendes, bombs, C4 and other non-handheld weapons. (INT + DEX)
Melee 		| Getting up-close (and personal heh). The ability for someone to swing a sword, throwing knife or bat (STR)
Medicine 	| Applying bandages to being House. (INT)


###Calculating Actions ###

Initiate = BaseActionPoints - CharacterActionPoints

Actions are anything that involve a character doing something during combat. Combat is based on a round time of 6 seconds. Every 6 seconds a player's action points "Recharge" resulting in their ability to do something - such as shoot, throw a grenade, hack a terminal or apply medicine. 

Each weapon, based on a variety of stats, impacts how many action points are required to in order to fire, throw or initiate. 

For example, for someone to fire a small gun requires 2 AP. Someone generates X AP every 'round', but lets say someone starts on 7 AP - every 6 seconds they will shoot - subtracting 2 AP from their pool of AP yet increasing it by 1 AP since its a new round. 

A rough equation to calculate length of how someone can shoot for: 

Total Action Points are calcuated off LUNGS * 1.5
TotalActionPoints (TAP)
Action Points Required (APR)


TAP = (TAP - APR) + 1 

-----
Different actions require different attributes. Below are the calculations based on type of weapon / action. Note that from a 'class' definition, rocket launchers would inherit a base class of action -> big gun -> rocket launcher.

----
#### Weapon Actions ####

Melee 				| 2 
Small Guns 			| 4
Big Guns 			| 8 - (STR / 2 )
Big Gun - 
Rocket launcher 	| 12 - (STR / 2)
Throwing 
(knife, grenade)   	| 6
Hacking				| 10 - (INT / 2)
----
#### Movement Actions ####

Crouch | 2
Prone  | 2 
Jump   | 5 * Distance - (STA  / 2)

----
#### General Actions ####

Open door & window  		 | 2 
Apply Bandage (Light wound)  | 7 - (INT / 2)
Apply Medicine (Heavy wound) | 9 - (INT / 2)
Setting Device (C4, Mine) 	 | 10 - (DEX / 4) + (INT / 4)
Detonating Device			 | 2 

##Environment##
The environment, within a tactical setting, is about cover and space. Distance is measured in real-time. There is no tactical grid, but instead it works within a range of 'meters' (though not a bad idea to turn it into feet for the sake of localization). 

The characters are encouraged to strategically move to obtain cover by way of crouching or proning behind objects. Cover impacts visibility and to-hit chance for both the character and the enemy combatant.  

###Light###
In the environment, light-levels become a factor. Light is on a spectrum from Max darkness (-5) to bright (0). (-5 Dark to 0 Max Bright)


###Cover Level###
Each environment object has a cover level and a destructability level. The cover level is based on 4 levels: 
none:0, light:20, medium:40, heavy:60, impenetrable:100 - different objects have different cover levels.

###Stance###
Stance is based on a spectrum. 0 is the floor, 1 is stance, 2+ is floors above. For example, prone is at the level of 0. 
For simplicity, if someone is behind a wall or a coverage area, it automatically equates to a crouch stance. If there's no cover, as in the character is out in the open, then the stance is considered to be 1.

For some additional stances, I've added in unconcsious, zerogravity,pinned and crazy. Stance is most likely going to be integral to animation and there could be more than just the standard combat movement - however it'll still contain the same value as prone, crouch or standing.'
###Elevation###
Elevation is based on a height. The height is based roughly on 3 meters per story. There is a max of 4 levels someone can situation themselves on and that can impact EYE and LOS. 

###Weapon Range Modifier###
A weapon's range sets a limit on how far a shot can travel before accuracy suffers. 

###Shooting from behind cover###
Shooting from behind things takes into account a number of factors. A character behind a cover-object will automatically crouch, reducing their to-hit automatically. 

###Calculating To-Hit###
To hit is based on a combination of elevation, light-level, distance, stance, skill and the weapon being used.

CoverLevel 		= enum (light, 20, medium, 40, heavy, 60, impenetrable, 100)
Elevation 		= enum (Floor: 0,Height:1, Height:2, Height:3) | each height equates to 3m 
Distance 		= Generally from a RayTrace; Requires a clear LOS. Converted to meters. 
Light 			= How dark vs bright it is 
Elevation 	 	= The elevation for both parties 
Cover  			= The level of cover the person being shot at 
Weapon 			= Different weapons have different modifiers
Stance 			= Whether crouched, unconscious, crazy,floating in zero-gravity, in the open or prone

Distance = 50m 
Light = 0 (Max Bright)
Elevation = 2
Cover (Checks LOS) = 40 (Medium)
Weapon = SMG (Small Gun, no modifier)

Attacker 		= Stance = 1

HitToBeat 		= CoverLevel + (DistanceToTarget + AdditionalDefenseModifiers) + (2 * LightLevel ) + (Elevation * 10) - (Stance * 5)

RND((WeponSkill / 2 + Perception * 2), WeaponSkill)
ChanceToHit 	= ( (RND((WeponSkill / 2 + Perception * 2), WeaponSkill)) + Specrum(Mental) + Specrum(Physical) + WeaponRangeModifier + WeaponModifer) + (Elevation * 10) - (Stance * 5)

Damage 			= GunTypeCategorySkill  * RND(GunTypeDamage - (GunTypeCategorySkill / 100) )   

e.g.

Dummy stats 
Small Guns: 77

Sniper
----
Brain 4
Muscles 4
Hands 6
Lungs 4
Heart 4 
Mouth 4
Eye 4


Scenario - Medium coverage, behind a wall. Target is 50 meters away.
Weapon: Pistol - 
Skill in Small Arms: 85 0 Mod Damage (15 - 22)
No weapon modifiers
No Attacker Character Modifiers
No Defense Character Modifiers
Mental State: 3 (out of -5 to 5)
Physical State: (2)
LightLevel: 0
Shooter Stance: 1
Shooter Elevation: 0
Target Stance: 1 
Target Elevation: 0


Total Hit Result = Distance + Cover + Light - Character + Gun + Modifiers

WeaponRangeModifer = (WeaponRange - Distance)
if (WeaponRangeModifer > 1) {
	WeaponRangeModifer = 0;
}

DefencePosition = CoverLevel + (DistanceToTarget + AdditionalDefenseModifiers + CharacterModifiers) + (2 * LightLevel ) + (Elevation * 10) - (Stance * 5)
AttackPosition = (EYE * 2) + ((RND(GunTypeCategorySkill / 100 - 1)) + Specrum(Mental) + Specrum(Physical) + WeaponModifer + CharacterModifiers) + (Elevation * 10) - (Stance * 5) + WeaponRangeModifer

Defender In Numbers: 40 + (50 + 0 + 0) + (2 * 0) + (0 * 10) - (1 * 5)
90 - 5 = 85
Attacker In Numbers: (4 * 2) + (77 + 3 + 2 + 0 + 0) + (0 * 10) - (1 * 5)
(8) + (80) + (0) - (5) = 85
ToHitResult = AttackPosition - DefencePositon 
bHit ? ToHitResult > 0 : true, false 

if (bHit) {
	Damage = RND(GunDamageLow to GunDamageHigh) * (GunTypeCategorySkill / 100) ) + WeaponModifiers
}else{
	Missed
}

##Tactical Layer##

Tactical layer consists of viewing the players 'ship' with various characters indicating what they're doing. 
Since the goal is to capture grid coordinates and 'land' - certain areas of land will give you a bonus to your income. This income is then distributed either with your 
characters, securing facilities or managing new characters. 

You can also address specific character's needs. For example, sending a character to the bar will help increase their mental state back to the positive. 

Initally a ship can: 

Train milita (Depends on teamlevel + Charisma to determine amount of milita trained)
Train a particular skill
Heal
Undergo surgery (implements fixes for physical nature) Physical
Drink at bar (helps team state if other players go with them) + Team
Backhome holoroom - + Mental
You can also buy and repair weapons

