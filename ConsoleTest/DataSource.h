#pragma once
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
/* Reads and writes from a file */
class DataSource
{
public:
	DataSource();
	/* Writes a map to CSV. Useful for character sheets if randomly generated via CharacterSetup */
	bool WriteOutputToCSV(std::map<std::string,float> *file);

	// String value
	bool WriteOutputToCSV(std::string stringValue);

	// File used for general logs
	std::ofstream logfile;

	// File used for outputting character info
	std::ofstream dataout;

	std::ifstream datacharacter;
	// File locations - assumption is that Data
	const std::string logLocation = "Data\\blackstarlog.txt";
	const std::string csvlocation = "Data\\blackstardataout.csv";
	const std::string charactersheetimport = "Data\\charactersheetimport.csv";
	/* Gets our CSV and converts it into a Map that we can assign to a character's attributes, skills,specrum and the name */
	std::vector<std::map<std::string, std::string>> DataSource::CSVToMap();
	
	
	
	/* Write to a log file */
	bool WriteToLog(std::vector<std::string> file);

	~DataSource();
};

