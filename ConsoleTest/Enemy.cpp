#include "stdafx.h"
#include "SetupCharacter.h"
#include "Enemy.h"


Enemy::Enemy()
{
	SetupCharacter setup;
	Name = setup.CreateName();
}

void Enemy::TakeDamage(float damageAmount)
{
	

	enemyHealth -= damageAmount;
	if (enemyHealth <= 0)
	{
		SetToDelete();
		enemyHealth = 0;
	}
}

Character* Enemy::AssessEnemyHealth(std::vector<Character*> *characterList)
{
	float targetHealth = 0;

	Character* foundCharacter = nullptr;

	for (auto it = characterList->begin(); it != characterList->end(); it++)
	{
		if ((*it)->GetHealth() > targetHealth) 
		{
			targetHealth = (*it)->GetHealth();

			foundCharacter = (*it);
		}
	}
	
	return foundCharacter;
}


void Enemy::Destroy()
{
	delete this;
}

float Enemy::Attack()
{
	
	return 5.0f;
}

Enemy::~Enemy()
{
}
