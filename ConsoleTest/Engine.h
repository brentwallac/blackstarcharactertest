#pragma once
#include "stdafx.h"
#include <vector>
#include "Character.h"
#include "Enemy.h"
#include <chrono>

enum Cover {
	COVERLOW = 20,
	COVERMEDIUM = 40,
	COVERHIGH = 60,
	COVERIMPENETRABLE = 500
};
struct Environment{
	signed int lightlevel;
	int elevation;
	int distance;
	enum Cover coverlevel;

};
class Engine
{
public:
	
	Engine(vector<Character*> *characters);
	
	/* Offers menu solutions for the user - Quit. change action etc */
	bool HandleEvents();

	const int NUM_ENEMIES = 2;

	vector<Enemy*> enemyList;
	// The Enemy Location
	Environment enemyLocation = { 0,2,25,COVERLOW };
	/* The loop of the game. Basic Loop timed on a chrono cycle.*/
	void GameLoop();
	// Returns our menu object
	//game_state interpolate(game_state const & current, game_state const & previous, float alpha);
	~Engine();
	float CalculateDamage(Character *Attacker);
	float CalculateToHitChance(Character *Attacker);
	//Menu menu;
	// A list of the characters
	vector<Character*> *characterList;

	/* A vector of class enemy that will be destroyed at an ideal time*/
	void EnemyDestroyQueue();
	/* Calculates the enemy's ability to attack */
	void EnemyTurn();

protected:
	
	
private: 
	// The time in seconds that constitutes a single round. 
	std::chrono::seconds roundTime;
	// Number of rounds. Used for rendering stuff
	int roundCount = 0;
	/*A simple function for checking if the user wants to quit */
	
	/*
	* Check to see what each person is doing, action-wise.
	* Then we assess, based off their action, what to do in this 'round'.
	* Borrowing from Infinity Engine, each 6 seconds constitutes a 'round', so each action is based
	* on what that means.
	* TODO: How does attack speed work? Are attacks based off attacks per round? Or is it # attacks per round
	* or x number of rounds to perform one attack?
	* TODO: Simply put, it comes down to producing whether someone hits / what damage or misses based off a number of variables.
	* variables such as a combination of the weapon + attributes + spectrum + environment placement.

	*/
	void Render();
};